package com.example.omarsharif.regform;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    private EditText editFirstName;
    private EditText editLastName;
    private EditText editPhone;
    private EditText editEmail;
    private EditText editPassword;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editFirstName = findViewById(R.id.firstName);
        editLastName = findViewById(R.id.lastName);
        editPhone = findViewById(R.id.phone);
        editEmail = findViewById(R.id.email);
        editPassword = findViewById(R.id.password);

    }

    public void sendToNext(View view) {

        String firstname = editFirstName.getText().toString();
        String lastName = editLastName.getText().toString();
        String phone = editPhone.getText().toString();
        String email = editEmail.getText().toString();
        String password = editPassword.getText().toString();

        Intent intent = new Intent(this, Main2Activity.class);

        String fullName = firstname + " " + lastName;

        intent.putExtra("name", fullName);
        intent.putExtra("phone", phone);
        intent.putExtra("email", email);
        intent.putExtra("password", password);

        startActivity(intent);
    }

    public void sendToEmail(View view) {
        String firstname = editFirstName.getText().toString();
        String lastName = editLastName.getText().toString();
        String phone = editPhone.getText().toString();
        String email = editEmail.getText().toString();
        String password = editPassword.getText().toString();

        String fullName = firstname + " " + lastName;

        Intent intent = new Intent(Intent.ACTION_SENDTO, Uri.parse("mailto"));

        intent.putExtra(Intent.EXTRA_EMAIL, "omarsharif439@gmail.com");
        intent.putExtra(Intent.EXTRA_SUBJECT, "My Details");
        intent.putExtra(Intent.EXTRA_TEXT, "Full Name: " + fullName +
                "\n\n\nEmail: " + email +
                "\n\n\nPhone: " + phone +
                "\n\n\nPassword: " + password);

        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
    }
}
